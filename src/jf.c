#include <stdio.h>
#include <stdlib.h>

#define BUFFER_SIZE (1024 * 4)

static void outc(char ch) {
  if (EOF == putchar(ch)) {
    exit(EXIT_FAILURE);
  }
}

static void fputs_repeat(const char *str, size_t count) {
  while (count-- > 0) {
    int value = fputs(str, stdout);
    if (value == EOF) {
      exit(EXIT_FAILURE);
    }
  }
}

int main(int argc, char **argv) {
  char buffer[BUFFER_SIZE];
  const char placeholder[] = "  ";
  unsigned int indent = 0;
  char is_string = 0;
  char escaped = 0;

  while (!feof(stdin)) {
    size_t n = fread(&buffer, sizeof(char), BUFFER_SIZE, stdin);

    if (n != BUFFER_SIZE && ferror(stdin)) {
      exit(EXIT_FAILURE);
    }

    for (size_t k = 0; k < n; k++) {
      char ch = buffer[k];

      if (is_string) {
        outc(ch);
        if (!escaped) {
          if (ch == '"') {
            is_string = 0;
          } else if (ch == '\\') {
            escaped = 1;
          }
        } else {
          escaped = 0;
        }
        continue;
      }

      switch (ch) {
      case ' ':
      case '\t':
      case '\n':
      case '\r':
        break;

      case '{':
      case '[':
        outc(ch);
        outc('\n');
        indent++;
        fputs_repeat(placeholder, indent);
        break;

      case '}':
      case ']':
        outc('\n');
        indent--;
        fputs_repeat(placeholder, indent);
        outc(ch);
        if (indent == 0)
          outc('\n');
        break;

      case ',':
        outc(',');
        outc('\n');
        fputs_repeat(placeholder, indent);
        break;

      case ':':
        outc(':');
        outc(' ');
        break;

      case '"':
        outc('"');
        is_string = 1;
        break;

      default:
        outc(ch);
        break;
      }
    }
  }

  return EXIT_SUCCESS;
}
