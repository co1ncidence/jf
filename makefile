CC     ?= gcc
CFLAGS += -pedantic -Wall -Wextra

NAME = jf
BIN  = bin/$(NAME)
MAN  = doc/$(NAME).1

PREFIX ?= $(DESTDIR)/usr
BINDIR  = $(PREFIX)/bin
MANDIR  = $(PREFIX)/share/man/man1

all : $(BIN)

bin/% : src/%.c
	@mkdir -p bin
	$(CC) $(CFLAGS) $< -o $@

clean :
	rm -rf bin

install : all
	install -Dm755 $(BIN) -t $(BINDIR)
	install -m 0644 doc/$(NAME).1 $(MANDIR)

uninstall :
	rm -f $(BINDIR)/$(NAME)
	rm -f $(MANDIR)/$(NAME).1

.PHONY : all clean install uninstall
